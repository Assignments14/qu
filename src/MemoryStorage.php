<?php

/**
 * We can use SPLObjectStorage but SQL query is simpler for quering
 */

namespace Rooslunn\QUA;

use Rooslunn\QUA\Contracts\Storage;
use stdClass;

final class MemoryStorage implements Storage
{
    private \PDO $dbh;
    private \PDOStatement $stmt;

    public function __construct()
    {
        $this->dbh = new \PDO('sqlite::memory:', null, null, $this->defaultOptions());
        $this->createCommandsTable();
    }

    private function createCommandsTable(): bool
    {
        $sql = 'create table commands (
            type char(1),
            service varchar(32),
            question varchar(32),
            response char(1),
            date varchar(32),
            time varchar(32)
        )';
        return $this->query($sql)->execute();
    }

    public function insert(array $data): bool
    {
        $sql = 'insert into commands 
            (type, service, question, response, date, time) values
            (:type, :service, :question, :response, :date, :time)';

        $query = $this->query($sql);

        foreach ($data as $field => $value) {
            $query->bind(":$field", $value);
        }

        return $this->execute();
    }

    public function query(string $sql): self
    {
        $this->stmt = $this->dbh->prepare($sql);
        return $this;
    }

    public function bind(string $param, $value, int $type = null): self
    {
        if (is_null($type)) {
            $type = $this->guessParamType($value);
        }
        $this->stmt->bindValue($param, $value, $type);

        return $this;
    }

    private function execute(): bool
    {
        return $this->stmt->execute();
    }

    public function fetchAll(): array
    {
        $this->execute();
        return $this->stmt->fetchAll();
    }

    public function fetchOne(): ?stdClass
    {
        $this->execute();
        return $this->stmt->fetch();
    }

    public function debugDump(): void
    {
        $this->stmt->debugDumpParams();        
    }

    private function guessParamType($value): int
    {
        switch (true) {
            case is_int($value):
                $result = \PDO::PARAM_INT;
                break;
            case is_bool($value):
                $result = \PDO::PARAM_BOOL;
                break;
            case is_null($value):
                $result = \PDO::PARAM_NULL;
                break;
            default:
                $result = \PDO::PARAM_STR;
        }

        return $result;
    }

    private function defaultOptions(): array
    {
        return [
            \PDO::ATTR_PERSISTENT => true,
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_OBJ,
        ];
    }


}